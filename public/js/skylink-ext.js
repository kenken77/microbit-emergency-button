/**
 * Created by Kenneth on 17/9/2016.
 */
var skylink = new Skylink(),
    $captures = document.getElementById('captures'),
    $btn = document.getElementById('captureBtn');

skylink.on('peerJoined', function(peerId, peerInfo, isSelf) {
    if(isSelf) return; // We already have a video element for our video and don't need to create a new one.
    var vid = document.createElement('video');
    vid.autoplay = true;
    vid.muted = true; // Added to avoid feedback when testing locally
    vid.id = peerId;
    vid.className = 'fullscreen-bg__video';
    console.log(vid);
    var vv = document.getElementById('myPeerVideo');
    vv.appendChild(vid);
    //$("myPeerVideo").append(vid);

});

skylink.on('incomingStream', function(peerId, stream, isSelf) {
    if(isSelf) return;
    var vid = document.getElementById(peerId);
    attachMediaStream(vid, stream);
});

skylink.on('peerLeft', function(peerId, peerInfo, isSelf) {
    var vid = document.getElementById(peerId);
    document.body.removeChild(vid);
});

skylink.on('mediaAccessSuccess', function(stream) {
    var vid = document.getElementById('myvideo');
    attachMediaStream(vid, stream);
});

skylink.init({
    apiKey: '780e49e7-ebb1-494e-82a3-0559cfc4b7e8',
    defaultRoom: 'Room1'
}, function() {
    skylink.joinRoom({
        audio: true,
        video: true
    });
});

function capture() {
    if(!$btn.disabled) {
        $status.innerText = "Capturing...";
        $btn.disabled = true;
        skylink.sendP2PMessage('/capture');
    }
}

$('.vid').each(function() {
    console.log($(this).id);
});

