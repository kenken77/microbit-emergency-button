var BBCMicrobit = require('bbc-microbit');
var path = require("path");
var express = require("express");
var redis = require('redis');
    client = redis.createClient(6379, '128.199.169.78');

var app = express();

var WatchJS = require("watchjs")
var watch = WatchJS.watch;
var async = require('async');
var sleep = require('sleep');

var bodyParser = require("body-parser");
var session = require("express-session");
var cookieParser = require('cookie-parser');

var moment = require('moment-timezone');
const SERVER_TZ = "Asia/Singapore";

const  PORT = "port";
var period = 160; // ms
var microbitTemperature = 0;

var buttonAState = {};
var buttonBState = {};

var pin8Value = 1;
var pin12Value =  1;
var pin0Value =  1;
var pin16Value = 1;

var directionChanges = {
    direction: 'S'
}

const PIN8 = 8;
const PIN12 = 12;
const PIN0 = 0;
const PIN16 = 16;


app.use(cookieParser());
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

// Initialize session
app.use(session({
  secret: "nus-stackup-iot",
  resave: false,
  saveUninitialized: true
}));

var BUTTON_VALUE_MAPPER = ['Not Pressed', 'Pressed', 'Long Press'];

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});


client.on("error", function (err) {
    console.log("Error " + err);
});

console.log('Scanning for microbit');
BBCMicrobit.discover(function(microbit) {
    console.log('\tdiscovered microbit: id = %s, address = %s', microbit.id, microbit.address);

	console.log('connecting to microbit');
	microbit.connectAndSetUp(function() {
		console.log('\tconnected to microbit');

		microbit.subscribeButtons(function() {
			console.log('\tsubscribed to buttons');
			sleep.usleep(1000000);
		});

		microbit.writeMagnetometerPeriod(period, function() {
			console.log('\tmagnetometer period set');

			console.log('subscribing to magnetometer');
			microbit.subscribeMagnetometer(function() {
				console.log('\tsubscribed to magnetometer');
			});
		});

		microbit.writeTemperaturePeriod(period, function() {
			console.log('\ttemperature period set');

			console.log('subscribing to temperature');
			microbit.subscribeTemperature(function() {
				console.log('\tsubscribed to temperature');
			});
		});

		console.log('setting accelerometer period to %d ms', period);
		microbit.writeAccelerometerPeriod(period, function() {
			console.log('\taccelerometer period set');

			console.log('subscribing to accelerometer');
			microbit.subscribeAccelerometer(function() {
				console.log('\tsubscribed to accelerometer');
			});

		});

		microbit.pinOutput(PIN8, function() {
			console.log('\tpin set as output');

			console.log('setting pin %d as digital', 8);
			microbit.pinDigital(PIN8, function() {
				console.log('\tpin set as digital ' +  pin8Value);
			});
		});

		microbit.pinOutput(PIN12, function() {
			console.log('\tpin set as output');

			console.log('setting pin %d as digital', 12);
			microbit.pinDigital(PIN12, function() {
				console.log('\tpin set as digital '  + pin12Value);
			});
		});

		watch(directionChanges, ["direction"], function(){
			console.log(directionChanges.direction);
			async.parallel({
					motorone: function(callback){
						setTimeout(function(){
							togglePin(PIN0, parseInt(pin0Value));
							togglePin(PIN16, parseInt(pin16Value));
							callback(null, 1);
						}, 200);
					},
					motortwo: function(callback){
						setTimeout(function(){
							togglePin(PIN8, parseInt(pin8Value));
							togglePin(PIN12, parseInt(pin12Value));
							callback(null, 2);
						}, 100);
					}
				},
				function(err, results) {
					console.log(results);
				});

		});

		microbit.pinOutput(PIN0, function() {
			console.log('\tpin set as output');

			console.log('setting pin %d as digital', 0);
			microbit.pinDigital(PIN0, function() {
				console.log('\tpin set as digital ' + pin0Value);
			});
		});

		microbit.pinOutput(PIN16, function() {
			console.log('\tpin set as output');

			console.log('setting pin %d as digital', 16);
			microbit.pinDigital(PIN16, function() {
				console.log('\tpin set as digital ' + pin16Value);
			});
		});

		function togglePin(pin, pinValue) {
			console.log('writing %d to pin %d', pinValue, pin);
			microbit.writePin(pin, pinValue, function() {
				console.log('\tdone');

			});
		}
	});

	microbit.on('disconnect', function() {
		console.log('\tmicrobit disconnected!');
	});

	microbit.on('accelerometerChange', function(x, y, z) {
		//console.log('\ton -> accelerometer change: accelerometer = %d %d %d G', x.toFixed(1), y.toFixed(1), z.toFixed(1));
		var accelerometerData = {};
		accelerometerData = { sensorType: 'PatientAccelerometer', accelerometerX: x.toFixed(1), accelerometerY: y.toFixed(1), accelerometerZ: z.toFixed(1)};
		client.hmset("PatientAccelerometerData", accelerometerData, function (err, res) {
			accelerometerData = {};
		});
	});

	microbit.on('buttonAChange', function(value) {
		console.log('\ton -> button A change: ', BUTTON_VALUE_MAPPER[value]);
		buttonAState = {sensorType: 'PatientButtonA', buttonAState: BUTTON_VALUE_MAPPER[value]};
		if(value != 0) {
			client.hmset("PatientButtonAState", buttonAState, function (err, res) {
				buttonAState = {};
			});
		}
	});

	microbit.on('buttonBChange', function(value) {
		console.log('\ton -> button B change: ', BUTTON_VALUE_MAPPER[value]);
		buttonBState = {sensorType: 'PatientButtonB', buttonBState: BUTTON_VALUE_MAPPER[value]};
		if(value != 0) {
			client.hmset("PatientButtonBState", buttonBState, function (err, res) {
				buttonBState = {};
			});
		}
	});

	microbit.on('magnetometerChange', function(x, y, z) {
		var magnetometerData = {};
		//console.log('\ton -> magnetometer change: magnetometer = %d %d %d', x.toFixed(1), y.toFixed(1), z.toFixed(1));
		magnetometerData = { sensorType: 'PatientMagnetometer',magnetometerX: x.toFixed(1), magnetometerY: y.toFixed(1), magnetometerZ: z.toFixed(1) };
		client.hmset("PatientMagnetometerData", magnetometerData, function (err, res) {
			magnetometerData = {};
		});
	});

	microbit.on('temperatureChange', function(temperature) {
		//console.log('\ton -> temperature change: temperature = %d °C', temperature);
		microbitTemperature = {sensorType: 'PatientTemperature', temperatureValue: temperature};;
		client.hmset("PatientMicrobitTemperature", microbitTemperature, function (err, res) {
			microbitTemperature = {};
		});
	});


});

app.use(express.static(__dirname + "/public"));
app.use("/bower_components", express.static(__dirname + "/bower_components"));

app.set(PORT, process.argv[2] || process.env.APP_PORT || 3001);

var server = app.listen(app.get(PORT) , function(){
  console.info("[Band] App Server started on " + app.get(PORT));
});