nodemon --nouse-idle-notification --expose-gc --max-old-space-size=2048 app.js

Use Zadig to install the WinUSB driver for your USB device. Otherwise you will get LIBUSB_ERROR_NOT_SUPPORTED when attempting to open devices.